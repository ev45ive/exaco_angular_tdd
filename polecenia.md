# Projekt
<!-- .gitconfig - usuwamy sekcje [proxy] -->
cd ..

git clone https://bitbucket.org/ev45ive/exaco_angular_tdd

cd exaco_angular_tdd/
npm install
npm run start

# API - json-server
https://jsonplaceholder.typicode.com/
https://github.com/typicode/json-server

npm i -g json-server
<!-- json-server moj_plik_z_danymi.json -->
json-server.cmd https://jsonplaceholder.typicode.com/db 
s + enter
json-server.cmd ./db-1580458187366.json

npm i --save json-server
json-server -> package.json -> scripts

# Interfaces
http://www.jsontots.com/
https://marketplace.visualstudio.com/items?itemName=MariusAlchimavicius.json-to-ts

## Edytor

https://marketplace.visualstudio.com/items?itemName=johnpapa.angular-essentials
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher

## Nowy projekt

File > Open Folder -> Tworzymy Folder
View > Terminal

npm i -g @angular/cli

cd ..
ng.cmd new <NAZWA_KATALOGU>

## Kreator projektu:

routing - Y
styles - SCSS

# Prettier + ESLint

https://prettier.io/docs/en/configuration.html
Ctrl + Shift + P => Create prettier doc

```json
{
  "tabWidth": 2,
  "useTabs": false,
  "singleQuote": true,
  "printWidth": 100
}
```

## Bootstrap CSS

npm install bootstrap --save

> angular.json > architect > build > styles

ng.cmd s -o

# Unit testy

ng.cmd test

# Karma

https://karma-runner.github.io/latest/index.html
https://github.com/karma-runner/karma-junit-reporter

# Device lab

devices lab remote
https://saucelabs.com/pricing

# Jasmine

https://jasmine.github.io/
https://mochajs.org/
http://qunitjs.com/

# Matchers / Assertions

https://www.chaijs.com/

# Test.js

# User Module

ng g m users --routing -m app
ng g c users/pages/login-page

# Jasmine

describe()
xdescribe()
fdescribe()

it()
xit()
fit()

# jasmine async tests / beforeEach blocks

Error: Timeout - Async callback was not invoked within 5000ms (set by jasmine.DEFAULT_TIMEOUT_INTERVAL)

# Zone.jS

https://github.com/angular/angular/tree/master/packages/zone.js

# AuthService

ng g s users/services/auth

# Fixtures

JSON import, tsconfig.json =>
"resolveJsonModule":true,
"allowSyntheticDefaultImports":true

https://bashooka.com/coding/10-javascript-tools-to-create-dummy-data-that-make-sense/

https://uifaces.co/
https://randomuser.me/
https://github.com/webpro/dyson
http://mockjs.com/
https://next.json-generator.com/
https://github.com/json-schema-faker/json-schema-faker
https://github.com/boo1ean/casual

# Blog - integration

https://jsfiddle.net/bk zp x4 nL/

Emmet / ZenCoding

ng g m blog --routing -m app
ng g c blog/views/search
ng g s blog/services/blog-api
ng g c blog/components/search-form
ng g c blog/components/posts-list
ng g c blog/components/post

## app.component.html
.container>.row>.col
.container>.row>.col>router-outlet

## blog/views/search.component.html
.row*2>.col

## blog/components/posts-list.component.html
.list-group>.list-group-item*3>app-post

## blog/components/search-form.component.html
.input-group>input.form-control+.input-group-append>button.btn.btn-outline-secondary{Search}

## blog/components/post.component.html
.media>.media-body>h5.mt-0{Title}+{Text}
.media>.media-body>img.mr-3+h5.mt-0{Title}+{Text}

## app.component.html
nav.navbar.navbar-expand.navbar-light.bg-light

a.navbar-brand{Title}

.collapse>ul.navbar-nav.mr-auto>li*2.nav-item>a.nav-link[href="asd"]{Text}

## OpenAPI / Swagger / CodeGen
https://swagger.io/
https://swagger.io/docs/specification/about/
https://github.com/swagger-api/swagger-codegen#building
https://www.npmjs.com/package/swagger-angular-generator


# marble testing
http://www.angular.love/2019/01/29/testowanie-rxjs-marble-diagrams/
https://medium.com/@bencabanes/marble-testing-observable-introduction-1f5ad39231c
https://stackblitz.com/edit/jasmine-marbles-testing?embed=1&file=src%2Fbasic.spec.ts&hideExplorer=1&source=post_page-----1f5ad39231c----------------------

# ngMocks
npm i --save ng-mocks

# NgNeat/Spectator
npm install @ngneat/spectator --save-dev

ng g @ngneat/spectator:spectator-component
ng g @ngneat/spectator:spectator-directive
ng g @ngneat/spectator:spectator-service


ng g @ngneat/spectator:spectator-component blog/views/post-view

ng g @ngneat/spectator:spectator-component blog/components/post-details --withHost=true

# Cypress
npm i cypress

cypress open

