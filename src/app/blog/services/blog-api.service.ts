import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../views/search/Post';

@Injectable({
  providedIn: 'root'
})
export class BlogApiService {
  constructor(private http: HttpClient /* , transformer: Tranformer */) {}

  fetchPostByIdWithComments(id: number) {
    return this.http.get<Post>(`http://localhost:3000/posts/${id}?_embed=comments`);
  }

  fetchPostById(id: number) {
    return this.http.get<Post>(`http://localhost:3000/posts/${id}`);
  }

  fetchRecentPosts() {
    return this.http.get<Post[]>('http://localhost:3000/posts/');
  }

  search(search: any): Observable<Post[]> {
    return this.http.get<Post[]>('http://localhost:3000/posts/?q=' + search);
  }
}
