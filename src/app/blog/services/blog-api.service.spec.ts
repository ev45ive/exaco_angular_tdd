import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { cold } from 'jasmine-marbles';
import { map } from 'rxjs/operators';
import { TestScheduler } from 'rxjs/testing';
import { mockPosts } from '../components/posts-list/mockPosts';
import { Post } from '../views/search/Post';
import { BlogApiService } from './blog-api.service';
import { mockComments } from './mockComments';

describe('BlogApiService', () => {
  let service: BlogApiService;
  let controller: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.get(BlogApiService);
    controller = TestBed.get(HttpTestingController);
  });

  afterEach(() => controller.verify());

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load recent posts from server', () => {
    service.fetchRecentPosts().subscribe(result => {
      expect(result).toEqual(mockPosts);
    });

    const r = controller.expectOne('http://localhost:3000/posts/');

    r.flush(mockPosts);
  });

  it('should load posts search results from server', () => {
    service.search('placki').subscribe(result => {
      expect(result).toEqual(mockPosts);
    });

    const r = controller.expectOne('http://localhost:3000/posts/?q=placki');

    r.flush(mockPosts);
  });

  it('should load post by id from server', () => {
    const id = 123;
    service.fetchPostById(id).subscribe(result => {
      expect(result).toEqual(mockPosts[0]);
    });

    const r = controller.expectOne(`http://localhost:3000/posts/${id}`);
    r.flush(mockPosts[0]);
  });

  it('should load post by id with comments from server', () => {
    const id = 123;
    const mockPostWithComments = mockPosts[0] as Post;
    mockPostWithComments.comments = mockComments;

    service.fetchPostByIdWithComments(id).subscribe(result => {
      expect(result).toEqual(mockPostWithComments);
      expect(result.comments).toEqual(mockComments);
    });

    const r = controller.expectOne(`http://localhost:3000/posts/${id}?_embed=comments`);
    r.flush(mockPostWithComments);
  });

  xit('should test rx operators', () => {
    const scheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });

    scheduler.run(({ cold, hot, expectObservable }) => {
      const values = { a: '123', b: 'placki' };
      // const actual = hot('  ----a|', values);

      const actual = cold('      ----a|', values);
      const subscriptions = '---^';
      const expected = '     --------a|';

      expectObservable(actual, subscriptions).toBe(expected, values);
    });
  });

  // jasmine-marbles
  xit('should test rx operators - jasmine-marble', () => {
    const values = { a: '123', b: 'placki' };
    const actual = cold('----a|', values).pipe(map(a => values.b));
    const expected = cold('----b|', values);

    expect(actual).toBeObservable(expected);
  });
});
