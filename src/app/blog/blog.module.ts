import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { SearchComponent } from './views/search/search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { PostsListComponent } from './components/posts-list/posts-list.component';
import { PostComponent } from './components/post/post.component';
import { PostViewComponent } from './views/post-view/post-view.component';
import { PostDetailsComponent } from './components/post-details/post-details.component';


@NgModule({
  declarations: [SearchComponent, SearchFormComponent, PostsListComponent, PostComponent, PostViewComponent, PostDetailsComponent],
  imports: [
    CommonModule,
    BlogRoutingModule
  ]
})
export class BlogModule { }
