import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { BlogApiService } from '../../services/blog-api.service';
import { Post } from '../search/Post';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.css']
})
export class PostViewComponent implements OnInit {
  post: Post;

  constructor(private route: ActivatedRoute, private api: BlogApiService) {}

  ngOnInit() {
    this.route.paramMap
      .pipe(
        map(p => p.get('id')),
        map(id => parseInt(id, 10)),
        // mergeMap, concatMap, exhaustMap
        switchMap(id => {
          return this.api.fetchPostByIdWithComments(id);
        })
        // o => o
      )
      .subscribe(response => {
        this.post = response;
      });
  }
}
