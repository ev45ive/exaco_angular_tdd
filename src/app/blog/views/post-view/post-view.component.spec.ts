import { ActivatedRoute } from '@angular/router';
import { createRoutingFactory, Spectator } from '@ngneat/spectator';
import { MockComponent } from 'ng-mocks';
import { NEVER, of } from 'rxjs';
import { PostDetailsComponent } from '../../components/post-details/post-details.component';
import { mockPosts } from '../../components/posts-list/mockPosts';
import { BlogApiService } from '../../services/blog-api.service';
import { PostViewComponent } from './post-view.component';

describe('PostViewComponent', () => {
  let spectator: Spectator<PostViewComponent>;

  // const createComponent = createComponentFactory({

  const createComponent = createRoutingFactory({
    component: PostViewComponent,
    mocks: [BlogApiService],
    componentMocks: [ActivatedRoute],
    declarations: [MockComponent(PostDetailsComponent)],
    // stubsEnabled: true,
    params: { id: 123 }
  });

  beforeEach(() => {
    spectator = createComponent({
      detectChanges: false // Delay ngOnInit
    });
    spectator
      .get(BlogApiService) //
      .fetchPostByIdWithComments.and.returnValue(NEVER);
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

  it('should request post data', () => {
    const apiSpy = spectator
      .get(BlogApiService) //
      .fetchPostByIdWithComments //
      .and.returnValue(of(mockPosts[0]));

    // Run ngOnInit
    spectator.detectChanges();

    expect(apiSpy).toHaveBeenCalledWith(123);
    expect(spectator.component.post).toEqual(mockPosts[0]);
  });

  it('should render post details', () => {
    // expect(spectator.query('app-post-details')).toExist()
    // const child = spectator.query('app-post-details');

    spectator.component.post = mockPosts[1];
    spectator.detectChanges();
    const child = spectator.query(PostDetailsComponent);

    expect('app-post-details').toExist();
    expect(child.post).toEqual(mockPosts[1]);
  });

  // it('should render post comments list', () => {});
});
