import { Component, OnInit } from '@angular/core';
import { BlogApiService } from '../../services/blog-api.service';
import { Post } from './Post';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  constructor(private service: BlogApiService) {}

  results: Post[];

  search(search: string) {
    this.service
      .search(search)
      .subscribe(results => (this.results = results));
  }

  ngOnInit() {}
}
