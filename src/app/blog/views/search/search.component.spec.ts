import { Component, EventEmitter, Input, Output } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Subject } from 'rxjs';
import { mockPosts } from '../../components/posts-list/mockPosts';
import { SearchFormComponent } from '../../components/search-form/search-form.component';
import { BlogApiService } from '../../services/blog-api.service';
import { Post } from './Post';
import { SearchComponent } from './search.component';

@Component({
  selector: 'app-search-form',
  template: '',
  providers: [
    {
      provide: SearchFormComponent,
      useExisting: FormMockComponent
    }
  ]
})
class FormMockComponent {
  @Output() search = new EventEmitter<string>();
}

@Component({
  selector: 'app-posts-list',
  template: ''
})
class PostsListMockComponent {
  @Input() posts: Post[];
}

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let service: jasmine.SpyObj<BlogApiService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchComponent, FormMockComponent, PostsListMockComponent],
      providers: [
        {
          provide: BlogApiService,
          useValue: jasmine.createSpyObj('MockAPI', ['search'])
        }
      ]
      // Shallow rendering:
      // schemas: [NO_ERRORS_SCHEMA]
      // schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    service = TestBed.get(BlogApiService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have search component', () => {
    // expect(fixture.debugElement.query(By.css('app-search-form'))).not.toBeNull(
    //   'Search form not found!'
    // );
    const child = fixture.debugElement.query(By.directive(SearchFormComponent));
    expect(child).not.toBeNull('Search form not found!');
  });

  it('should have results component', () => {
    expect(fixture.debugElement.query(By.css('app-posts-list'))).not.toBeNull('Results not found!');
  });

  it('should display search results', () => {
    const child = fixture.debugElement.query(By.css('app-posts-list'));

    // Fake search results in parent
    component.results = mockPosts as Post[];

    // Update child, check if it Got the results from parent
    fixture.detectChanges();
    expect(child.componentInstance.posts).toEqual(mockPosts);
  });

  it('form should search', () => {
    const child = fixture.debugElement.query(By.css('app-search-form'));

    spyOn(component, 'search');

    child.componentInstance.search.emit('Placki');

    expect(component.search).toHaveBeenCalledWith('Placki');
  });

  it('should search', () => {
    const fakeResponse = new Subject<Post[]>();
    service.search.and.returnValue(fakeResponse);

    component.search('Awesome');

    fakeResponse.next(mockPosts as Post[]);

    expect(service.search).toHaveBeenCalledWith('Awesome');
    expect(component.results).toEqual(mockPosts as Post[]);
  });
});
