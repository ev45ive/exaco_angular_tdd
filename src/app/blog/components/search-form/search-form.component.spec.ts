import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { SearchFormComponent } from './search-form.component';

@Component({
  template: `
    <app-search-form (search)="searchMock($event)"> </app-search-form>
  `
})
class ParentMockComponent {
  searchMock: jasmine.Spy;
}

describe('SearchFormComponent', () => {
  let parent: ParentMockComponent;
  let component: SearchFormComponent;
  let fixture: ComponentFixture<ParentMockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchFormComponent, ParentMockComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentMockComponent);
    parent = fixture.componentInstance;
    component = fixture.debugElement.query(By.directive(SearchFormComponent)).componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should search when button clicked', () => {
    const input = fixture.debugElement
      .query(By.directive(SearchFormComponent))
      .query(By.css('input'));

    const searchButton = fixture.debugElement
      .query(By.directive(SearchFormComponent))
      .query(By.css('button'));

    spyOn(component, 'doSearch');

    input.nativeElement.value = 'Placki123';
    searchButton.nativeElement.click();

    expect(component.doSearch).toHaveBeenCalledWith('Placki123');
  });

  it('should emit new searches to parent', () => {
    parent.searchMock = jasmine.createSpy('MockParentSearch');

    component.doSearch('Placki');

    expect(parent.searchMock).toHaveBeenCalledWith('Placki');
  });
});
