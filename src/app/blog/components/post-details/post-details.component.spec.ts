import { createHostFactory, SpectatorHost } from '@ngneat/spectator';
import { mockPosts } from '../posts-list/mockPosts';
import { PostDetailsComponent } from './post-details.component';

describe('PostDetailsComponent', () => {
  let spectator: SpectatorHost<PostDetailsComponent>;

  const createHost = createHostFactory(PostDetailsComponent);

  beforeEach(() => {
    spectator = createHost(`<app-post-details [post]="postData"></app-post-details>`, {
      hostProps: {
        postData: mockPosts[0]
      }
    });
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

  it('should get post data from parent', () => {
    // spectator.typeInElement('placki','.zippy__title');
    // expect(spectator.query('.arrow')).toHaveText('Close');
    expect(spectator.component.post).toEqual(mockPosts[0]);
  });

  it('should render post data from parent', () => {
    expect('.title').toHaveText(mockPosts[0].title);
    expect('.body').toHaveText(mockPosts[0].body);
  });
});
