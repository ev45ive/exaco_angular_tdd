import { Component, CUSTOM_ELEMENTS_SCHEMA, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Post } from '../../views/search/Post';
import { mockPosts } from './mockPosts';
import { PostsListComponent } from './posts-list.component';

@Component({
  selector: 'app-post',
  template: ``
})
class PostMockComponent {
  @Input() post: Post;
}

@Component({
  template: `
    <app-posts-list [posts]="data"></app-posts-list>
  `
})
class ParentMockComponent {
  data: Post[];
}

describe('PostsListComponent', () => {
  let fixture: ComponentFixture<ParentMockComponent>;
  let parentComponent: ParentMockComponent;
  let component: PostsListComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ParentMockComponent, PostsListComponent, PostMockComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentMockComponent);
    parentComponent = fixture.componentInstance;
    component = fixture.debugElement.query(By.css('app-posts-list')).componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get posts from parent', () => {
    parentComponent.data = mockPosts as Post[];
    fixture.detectChanges();
    expect(component.posts).toEqual(mockPosts as Post[]);
  });

  it('should render posts with post data', () => {
    const postsData = mockPosts as Post[];
    component.posts = postsData;
    // parentComponent.data = postsData;
    fixture.detectChanges();

    const posts = fixture.debugElement.query(By.css('app-posts-list')).queryAll(By.css('app-post'));
    expect(posts.length).toEqual(postsData.length);

    postsData.forEach((e, index) => {
      expect(posts[index].componentInstance.post).toEqual(postsData[index]);
    });
  });

});
