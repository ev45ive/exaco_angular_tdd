import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { Post } from '../../views/search/Post';
import { mockPosts } from '../posts-list/mockPosts';
import { PostComponent } from './post.component';

// Mock Parent
@Component({
  template: `
    <app-post [post]="data"></app-post>
  `
})
class ParentMockComponent {
  data: Post;
}

// Mock Sibling
@Component({
  template: ``
})
class SiblingMockComponent {}

describe('PostComponent', () => {
  let parentComponent: ParentMockComponent;
  let component: PostComponent;
  let fixture: ComponentFixture<ParentMockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostComponent, ParentMockComponent, SiblingMockComponent],
      // mocks:[Router],
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: 'posts/:id',
            component: SiblingMockComponent
            // canActivate:[ AuthGuard ]
          }
        ])
      ],
      providers: [
        // {
        //   provide: Router,
        //   useValue: jasmine.createSpyObj<Router>('Router', ['navigate'])
        // }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    /* ??? */

    fixture = TestBed.createComponent(ParentMockComponent);
    parentComponent = fixture.componentInstance;
    component = fixture.debugElement.query(By.directive(PostComponent)).componentInstance;
    parentComponent.data = mockPosts[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should recieve post data from parent', () => {
    parentComponent.data = mockPosts[1];
    fixture.detectChanges();
    expect(component.post).toEqual(mockPosts[1]);
  });

  it('should render post', () => {
    component.post = mockPosts[1];
    fixture.detectChanges();

    // title
    const title = fixture.debugElement.query(By.directive(PostComponent)).query(By.css('.title'));
    expect(title).not.toBeNull();
    expect(title.nativeElement.textContent).toMatch(mockPosts[1].title);
    // body
    const body = fixture.debugElement.query(By.directive(PostComponent)).query(By.css('.body'));
    expect(body).not.toBeNull();
    expect(body.nativeElement.textContent).toMatch(mockPosts[1].body);
  });

  it('should navigate to post details when title clicked', async(() => {
    // spectator.click('.title')
    component.post = mockPosts[1];
    fixture.detectChanges();

    const title = fixture.debugElement.query(By.css('.title'));
    title.triggerEventHandler('click', {});

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect((TestBed.get(Location) as Location).path()).toEqual('/posts/' + component.post.id);
    });
  }));
});
