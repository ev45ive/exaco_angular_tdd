import { Component, OnInit, Inject } from '@angular/core';
import { ILogin } from './ILogin';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  message: string;

  rememberMe = true;

  username: string;
  password: string;

  constructor(
    private router: Router,
    private auth: AuthService
  ) {}

  setMessage(message: string) {
    this.message = message;
    setTimeout(() => {
      this.message = '';
    }, 3000);
  }

  authenticate() {
    const { username, password } = this;

    this.auth
      .login({
        username,
        password
      })
      .subscribe({
        next: () => {
          this.message = 'Epic success!';
          this.router.navigate(['/']);
        }
      });
  }

  ngOnInit() {}
}
