import { Observable } from 'rxjs';

export type LoginPayload = {
  username: string;
  password: string;
};

type LoginResponse = {

};

export interface ILogin {
  login: (credentials: LoginPayload) => Observable<LoginResponse>;
}
