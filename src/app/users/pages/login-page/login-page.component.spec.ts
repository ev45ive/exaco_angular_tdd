import { DebugElement } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NEVER, of } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { ILogin } from './ILogin';
import { LoginPageComponent } from './login-page.component';


describe('LoginPageComponent', () => {
  let component: LoginPageComponent;
  let debugElement: DebugElement;
  let fixture: ComponentFixture<LoginPageComponent>;
  let router: jasmine.SpyObj<Router>;
  let authService: jasmine.SpyObj<ILogin>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginPageComponent],
      imports: [FormsModule],
      providers: [
        {
          provide: AuthService,
          useValue: jasmine.createSpyObj<AuthService>('AuthService', ['login'])
        },
        {
          provide: Router,
          useValue: jasmine.createSpyObj<Router>('Router', ['navigate'])
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
    router = TestBed.get(Router);

    authService = TestBed.get(AuthService);
    authService.login.and.returnValue(NEVER);
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should show success message', () => {
    // Arrange
    component.message = 'Login successful';
    // Act
    fixture.detectChanges();
    // Assert
    expect(
      (debugElement.query(By.css('.message')).nativeElement as HTMLElement).textContent
    ).toMatch('Login successful', 'No success message');
  });

  it('should show error message', () => {
    component.message = 'Login unsuccessful';

    fixture.detectChanges();
    expect(
      (debugElement.query(By.css('.message')).nativeElement as HTMLElement).textContent
    ).toMatch('Login unsuccessful');
  });

  it('should hide success message after 3sec', fakeAsync(() => {
    // Arrange: Given ...
    component.setMessage('');
    fixture.detectChanges();
    expect(debugElement.query(By.css('.message'))).toBeNull();

    // Immidiately ...
    component.setMessage('Login successful');
    fixture.detectChanges();
    expect(debugElement.query(By.css('.message'))).not.toBeNull();

    // After 1sec...
    tick(2000);
    fixture.detectChanges();
    expect(debugElement.query(By.css('.message'))).not.toBeNull();

    // After 3sec...
    // flushMicrotasks()
    tick(1000);
    // flush()
    fixture.detectChanges();
    expect(debugElement.query(By.css('.message'))).toBeNull();
  }));

  it('should have login and password inputs', () => {
    expect(debugElement.query(By.css('.login')).nativeElement as HTMLElement).toBeDefined();
    expect(debugElement.query(By.css('.password')).nativeElement as HTMLElement).toBeDefined();
  });

  it('should have login button', () => {
    expect(debugElement.query(By.css('.login-button')).nativeElement as HTMLElement).toBeDefined();
  });

  it('should login when login button is clicked', () => {
    // Arrange
    const buttonEl = debugElement.query(By.css('.login-button'));
    // component.authenticate = jasmine.createSpy('LoginButtonSpy');
    const spy = spyOn(component, 'authenticate');

    // Act
    buttonEl.triggerEventHandler('click', {});

    // Assert
    // expect(component.authenticate).toHaveBeenCalled();
    expect(spy).toHaveBeenCalled();
  });

  // it('should authenticate with login and password', inject(['AuthService'], (auth) => {
  it('should authenticate with login and password', () => {
    const loginElem = debugElement.query(By.css('.login')).nativeElement as HTMLInputElement;
    const passwordElem = debugElement.query(By.css('.password')).nativeElement as HTMLInputElement;
    // const buttonEl = debugElement.query(By.css('.login-button'));

    loginElem.value = 'TestUser';
    loginElem.dispatchEvent(new CustomEvent('input'));

    passwordElem.value = 'TestPa$$word';
    passwordElem.dispatchEvent(new CustomEvent('input'));
    // loginElem.triggerEventHandler('input', { target: loginElem.nativeElem });

    // buttonEl.triggerEventHandler('click', {});
    component.authenticate();

    expect(authService.login).toHaveBeenCalledWith({
      username: 'TestUser',
      password: 'TestPa$$word'
    });
  });

  it('should display message and redirect on successful login', () => {
    component.username = 'Placki';
    component.password = 'PlackiPassword';

    authService.login.and.returnValue(of('Placki'));

    component.authenticate();

    expect(component.message).toEqual('Epic success!');
    expect(router.navigate).toHaveBeenCalledWith(['/']);
  });

  it('should remember me option that is checked', async(() => {
    // return fixture.whenRenderingDone().then(() => {
    fixture.whenStable().then(() => {
      const rememberMeElem = debugElement.query(By.css('.rememberMe'));

      // expect(fixture.isStable).toBeTruthy()
      expect(rememberMeElem.nativeElement.checked).toBeTruthy();
    });
  }));
});

/*
describe('submodule',()=>{
  beforeEach(() => {
    console.log('inner')
  })

  it('should create', () => {
    expect(component).toBeDefined();
  });
})
 */

// @Inject(IJakaxinnaNazwa)
//  MyService implements IRobieX, IPotrafieY, IEmitujeZ
