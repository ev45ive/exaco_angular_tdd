import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpBackend } from '@angular/common/http';
import { AuthService } from './auth.service';
// import { mockCredentials } from './mockCredentials';
// tsconfig -> resolveJsonModule
import mockCredentials from './fixtures/mockCredentials.json';
import { Router } from '@angular/router';

describe('AuthService', () => {
  let service: AuthService;
  let controller: HttpTestingController;
  let router: jasmine.SpyObj<Router>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        {
          provide: Router,
          useValue: jasmine.createSpyObj<Router>('Router', ['navigate'])
        }
      ],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.get(AuthService);
    controller = TestBed.get(HttpTestingController);
    router = TestBed.get(Router);
  });

  afterEach(() => controller.verify());

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should log user in', () => {
    service.login(mockCredentials).subscribe({
      next: resp => {
        expect(resp).toEqual({ success: true });
      }
    });

    // controller.expectOne('/autoryzujUżytkownika', 'Login endpoint');
    const loginRequest = controller.expectOne(
      {
        method: 'POST',
        url: '/autoryzujUżytkownika'
      },
      'Login endpoint'
    );
    // expect(loginRequest.request.params).toEqual(...);
    // expect(loginRequest.request.headers).toEqual(...);
    expect(loginRequest.request.body).toEqual(mockCredentials);

    loginRequest.flush({
      success: true
    });
  });

  it('should log user out and redirect to login page', () => {
    service.logout();

    const req = controller.expectOne('/wylogujUżyszkodnika', 'Logout endpoint');

    req.flush(null);

    expect(router.navigate).toHaveBeenCalledWith(['/login']);
  });
});
