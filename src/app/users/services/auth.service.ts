import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ILogin, LoginPayload } from '../pages/login-page/ILogin';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements ILogin {
  logout() {
    return this.http.post('/wylogujUżyszkodnika', {}).subscribe(() => {
      this.router.navigate(['/login']);
    });
  }

  constructor(private router: Router, private http: HttpClient) {}

  login(credentials: { username: string; password: string }) {
    return this.http.post('/autoryzujUżytkownika', credentials);
  }
}
