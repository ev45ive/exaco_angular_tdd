import { browser, by, element } from 'protractor';
describe('Search', () => {
  beforeEach(() => {
    browser.get('/search');
  });

  it('type search query', () => {
    element(by.className('form-control')).sendKeys('sunt');

    element(by.buttonText('Search')).click();

    element(by.css('app-post .title')).click();

    browser.wait(() => element('app-post-details'), 1000);

    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/posts/1');

    // browser.debugger();
    // browser.pause(9229);
    // debugger
    // browser.sleep(100000);
  });
});
